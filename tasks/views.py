from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required


@login_required
def create_task(request):
    if request.method == "POST":
        create_task_form = TaskForm(request.POST)
        if create_task_form.is_valid():
            create_task_form.save()
            return redirect("list_projects")

    else:
        create_task_form = TaskForm()

    context = {
        "create_task_form": create_task_form,
    }

    return render(request, "tasks/create_task.html", context)


@login_required
def show_my_tasks(request):
    my_tasks = Task.objects.filter(assignee=request.user)
    context = {"my_tasks": my_tasks}
    return render(request, "tasks/show_my_tasks.html", context)
