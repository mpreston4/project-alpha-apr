from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


@login_required
def list_projects(request):
    projects_list = Project.objects.all().filter(owner=request.user)
    context = {
        "projects_list": projects_list,
    }
    return render(request, "projects/projects_list.html", context)


@login_required
def show_project(request, id):
    show_project = Project.objects.get(id=id)
    context = {"show_project": show_project}
    return render(request, "projects/show_project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        create_project_form = ProjectForm(request.POST)
        if create_project_form.is_valid():
            create_project_form.save()
            return redirect("list_projects")

    else:
        create_project_form = ProjectForm()

    context = {
        "create_project_form": create_project_form,
    }

    return render(request, "projects/create_project.html", context)
